#include <fstream>
#include <iostream>
using namespace std;
ifstream fic;

struct Maillon {
  int col;       // numéro de la colonne à laquelle correspond le coefficient
  int coef;      // coefficient de la matrice
  Maillon *suiv; // élément suivant non nul sur la ligne
};

struct MatriceAdjacence {
  int ordre;        // nombre de sommets du graphe
  Maillon **lignes; // tableau à allouer de taille "ordre", représentant les
                    // lignes de la matrice
};

// Creation de la matrice
void creerMatrice(MatriceAdjacence &m, int taille) {
  m.ordre = taille;
  m.lignes = new Maillon *[m.ordre];
  for (int i = 0; i < m.ordre; i++) {
    m.lignes[i] = nullptr;
  }
}

/* une fonction qui charge la matrice
  a partir d'un lecture d'un fichier
  et renvoie false s'il le trouve pas
*/

bool charger(char *nomFichier, MatriceAdjacence &mat) {

  fic.open(nomFichier, ios::in);
  if (!fic.is_open()) {
    cout << "Erreur d'ouvertur de fichier !!!";
    return false;
  }

  else {
    fic >> mat.ordre;
    creerMatrice(mat, mat.ordre);

    for (int i = 0; i < mat.ordre; i++) {
      for (int j = 0; j < mat.ordre; j++) {
        int coeficient;
        fic >> coeficient;

        if (coeficient != 0) {
          Maillon *maillon = new Maillon;
          maillon->col = j;
          maillon->coef = coeficient;

          if (mat.lignes[i] == nullptr) {
            mat.lignes[i] = maillon;
          } else {
            Maillon *ptr = mat.lignes[i];
            Maillon *prec = nullptr;

            while (ptr != nullptr) {
              prec = ptr;
              ptr = ptr->suiv;
            }
            prec->suiv = maillon;
          }
        }
      }
    }
  }
  fic.close();
  return true;
}

// hypothèses :
// - mat contient une matrice
// - l et c sont des coordonnées valides
// Fonction qui prend le coefficient de la matrice
int getCoeff(MatriceAdjacence mat, int l, int c) {

  Maillon *ptr = mat.lignes[l];
  for (int i = 0; i < mat.ordre; i++) {

    if (ptr != nullptr) {
      if (ptr->col == c) {
        return ptr->coef;
      } else
        ptr = ptr->suiv;
    }
  }
  return 0;
}

// une fonction qui permet d'effacer en memoire la matrice
void effacerMatrice(MatriceAdjacence &mat) {
  Maillon *ptr;
  for (int i = 0; i < mat.ordre; i++) {

    while (mat.lignes[i] != nullptr) {
      ptr = mat.lignes[i];
      mat.lignes[i] = mat.lignes[i]->suiv;
    }
  }

  delete ptr;
  mat.ordre = 0;
}

// Fonction pour l'affichage de la matrice
void afficher(MatriceAdjacence mat) {

  for (int i = 0; i < mat.ordre; i++) {
    Maillon *ptr = mat.lignes[i];

    for (int j = 0; j < mat.ordre; j++) {

      if (ptr != nullptr && ptr->col == j) {
        cout << ptr->coef << " ";
        ptr = ptr->suiv;
      } else
        cout << 0 << " ";
    }
    cout << endl;
  }
}

/* une fonction qui verifie si la matrice est egal
  à sa transposé => matrice symetrique */
bool estSymetrique(MatriceAdjacence mat) {

  for (int i = 0; i < mat.ordre; i++) {
    for (int j = 0; j < mat.ordre; j++) {

      if (getCoeff(mat, i, j) != getCoeff(mat, j, i)) {
        return false;
      }
    }
  }

  return true;
}

// une fonction qui verifie si la matrice est complet ou pas
bool estComplet(MatriceAdjacence mat) {

  for (int i = 0; i < mat.ordre; i++) {
    for (int j = 0; j < mat.ordre; j++) {

      if (i != j) {
        if (getCoeff(mat, i, j) == 0)
          return false;
      }
    }
  }

  return true; // aucun 0 trouvé
}

int main(int argc, char *argv[]) {

  if (argc != 2) {
    printf("Erreur - il manque le nom du fichier à lire\n");
    return -1;
  }

  MatriceAdjacence mat;

  if (!charger(argv[1], mat))
    return -1;
  else
    afficher(mat);
  cout << endl;

  cout << "## Operation sur la matrice ##" << endl;
  cout << "  ##########################" << endl << endl;
  if (estSymetrique(mat))
    cout << " La matrice est symétrique" << endl;
  else
    cout << " La matrice est non symétrique" << endl;

  if (estComplet(mat))
    cout << " La matrice est complet" << endl;
  else
    cout << " La matrice est incomplet." << endl;

  // effacerMatrice(mat);
  // afficher(mat);
  int lig, col;
  cout << " Donner la ligne à chercher son coefficient :";
  cin >> lig;
  cout << " Donner la colone à chercher son coefficient : ";
  cin >> col;

  cout << " val à la [" << lig << ";" << col << "] = ";
  cout << getCoeff(mat, lig, col);
  cout << endl;

  return 1;
}
