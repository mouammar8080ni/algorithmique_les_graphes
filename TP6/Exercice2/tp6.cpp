#include "types.hpp"
#include "matrices.hpp"
#include "parcours.hpp"

#include <iostream>
#include <fstream>
using namespace std;

// ligne suivante à décommenter pour l'exercice 2
#define GRAPHIQUES

#ifdef GRAPHIQUES
#include "graphique.hpp"
#endif






// === PROGRAMME PRINCIPALE ==== 
int main(int argc, char *argv[]){

  // affichages 
  cout<<" ==== MOUAMMAR SOULE ===="<<endl<<endl;;
  
  if(argc!=3){
    cout << "Syntaxe : " << argv[0] << " <file>  <indice_sommet_depart>" << endl;
    return -1;
  }

  MatriceAdjacence mat;

  if(!charger(argv[1], mat)) return -1;

  int sommetDepart = atoi(argv[2]);
  if(sommetDepart <0 || sommetDepart >= mat.ordre){
    cout << "indice de sommet " << sommetDepart << " incorrect ! ";
    cout << "valeurs autorisées dans [0," << mat.ordre-1 << "]" << endl;
    effacerMatrice(mat);
    return -1;
  }
  
  cout<<" ===  GRAPHE === "<<endl;
  afficher(mat);
  cout<<endl;
  cout<<" === DISTANCES ET CHEMIN ==="<<endl;

  // créer les tableaux à remplir par le parcours en largeur
  bool *traites = new bool[mat.ordre];
  float *distances = new float[mat.ordre];
  int *parents = new int[mat.ordre];
  Couleur *couleurs = new Couleur[mat.ordre];

  rechercheCheminMinimum(mat, distances, parents, couleurs, sommetDepart);
  affichages(mat, distances, parents, sommetDepart);
 cout<<endl;
     cout<<" === Distance d'un sommet à un autre ==="<<endl;
    for (int x = 0; x < mat.ordre; x++) {
      if (x!= sommetDepart && couleurs[x]==NOIR) {
          cout << "chemin vers " <<x <<"( lg = "<<distances[x]<<" ) = ";
          afficherCheminVers(x, parents);
      cout << endl;
   }
     
}


#ifdef GRAPHIQUES
  // ------ affichages graphiques -----

  // création de la fenêtre
  if(createWindow(mat)==EXIT_FAILURE){
    cout << "erreur de création de la fenêtre" << endl;
    return 0;
  }

  // affichage du graphe
  drawGraph(mat);

  // affichage des plus courts chemins
  drawPaths(mat, parents);

  // attendre l'appui sur une touche ou la fermeture de la fenêtre
  waitForEnd();

  // destruction de la fenêtre
  destroyWindow();
  
#endif

  // supprimer les tableaux
  delete traites;
  delete distances;
  delete parents;

  return -1;

}

