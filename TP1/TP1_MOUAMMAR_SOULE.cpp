#include <iostream>
#include<ctime>
#include<iomanip>
#include<fstream>
using namespace  std;

ifstream fic;

struct MatriceAdjacent{
    int ordre; // nombre de sommet 
    int *coef;// tableau contenant les ordres 
};

// Fonction qui creer une matrice adjcent 
// remplit de zero au depart avec sa taille

void creerMatrice(MatriceAdjacent &mat, int size){
    mat.ordre = size;
    mat.coef = new int[mat.ordre * mat.ordre];

   for(int i = 0; i < mat.ordre; i++){
     for(int j=0; j< mat.ordre; j++){
         mat.coef[i *mat.ordre + j] = 0;
     }
    }
      
}

// affichage de la matrice d'adjacent 
void afficher(MatriceAdjacent mat){

   for(int i = 0; i < mat.ordre; i++){
     for(int j=0; j< mat.ordre; j++){
         cout<<mat.coef[i *mat.ordre + j]<<" ";
     }
  //   cout<<endl;
   }
  cout<<endl;
}

/* une fonction qui consiste à 
  lire un fichier qui contient des matrice 
  la fonction renverra false si le fichier 
  n'est pas ouvert avec un sms d'erreur
*/

bool charger(char *nomFichier, MatriceAdjacent &mat){
      fic.open(nomFichier, ios::in);
      if(!fic.is_open()){
        cout<<" Erreur d'ouverture "<<endl;
        return false;
      }

    else {
       fic>>mat.ordre;
       //cout<<mat.ordre;
      //mat.coef = new int[mat.ordre * mat.ordre];
      creerMatrice(mat, mat.ordre);
     for(int i = 0; i < mat.ordre; i++){
       for(int j=0; j< mat.ordre; j++){
          fic>> mat.coef[i *mat.ordre + j];
       }
    }
      
    }
    fic.close();
  return true;
    
}

// Une fonction boolean qui verifie 
// si une matrice est complet ou pas
bool estComplet(MatriceAdjacent mat){

    for(int i=0; i < mat.ordre; i++){
      for(int j=0; j<mat.ordre; j++){
        
         if(i != j){
            if(mat.coef[i* mat.ordre + j] == 0) {
               return false;
            } 
         }
      }

    }
  return true;
}

//Une fonction qui permet de verfier si une matrice
// est symetrique = a sa transposé

bool estSymetrique(MatriceAdjacent mat){

    for(int i=0; i < mat.ordre; i++){
      
      for(int j=0; j<mat.ordre; j++){
        
            if(mat.coef[i* mat.ordre + j] != 
              mat.coef[j* mat.ordre + i]){
               return false;
         }
      }

    }
  return true;
}

// Creation d'une matrice qui verifie si un noeud 
//est seul(isolé), qu'il n'est pas connecté a aucun sommet

bool estIsole(MatriceAdjacent mat, int i){

      for(int j=0; j<mat.ordre; j++){
        // if( j != i){
           if(mat.coef[j * mat.ordre + i] != 0){
              return false;
            }
        // } 
      }
  return true;
}

// une fonction qui permet d'afficher les sommets 
// isolé ou pas en appel de la fonction 'estIsoler'

void afficherIsoler(MatriceAdjacent mat, int i){
  
    for (int i=0; i<mat.ordre; i++){
       if(estIsole(mat,i)){
         cout<<"Le sommet "<<i<< " est isolé"<<endl;
       } 
       else cout<<"Le sommet "<<i <<" n'est pas isolé "<<endl;
     }
   
}


// Fonction qui affiche le degret de chaque 
// sommet d'u graphe non orienté 
void afficherDegresNonOriente(MatriceAdjacent mat){

   for(int i =0; i< mat.ordre; i++){
       int degre = 0;
     for(int j = 0; j< mat.ordre; j++){
       degre = degre + mat.coef[i * mat.ordre + j];       
     }
     cout<<" Le sommet "<<i<<" possede "<<degre<<" degré"<<endl;
   }
}

//Une fonction qui affiche les degré entrant 
// et sortant des sommet d'un graphe orienté 

void afficherDegresOriente(MatriceAdjacent mat){
 int total = 0;
   for(int i =0; i< mat.ordre; i++){
       int degre = 0; 
     for(int j = 0; j< mat.ordre; j++){
       degre = degre + mat.coef[i * mat.ordre + j] ;    
      
     }
     if(mat.coef[i * mat.ordre +i ] == 1){
        degre =degre + 1;
     }


     total = degre;
     cout<<" Le sommet "<<i<<" possede "<<total<<" arrete"<<endl;
     // cout<<" Le sommet "<<i<<" possede "<<degreSortant<<" degré sortant"<<endl;
   }
}




// Programme Principale
int main(int argc, char *argv[]){
  srand(time(NULL));
  
  MatriceAdjacent mat;
  if( argc !=2 ){
    cout<<" erreur de parametre "<<endl;
    return 0;
  }
  
  char *fichier = argv[1];
  bool etat;
  //Chargement de la matrice et affichage
  charger(fichier, mat);
  afficher(mat);

  // Appel des fonctions 
  if(estComplet(mat)) cout<<" La matrice est complet "<<endl;
  else cout<<" La matrice est non-complet "<<endl;

  
  if (estSymetrique(mat)) cout<<" La matrice est symetrique "<<endl;
  else cout<<" La matrice n'est pas symetrique "<<endl;

  afficherIsoler(mat,0);
 
  afficherDegresNonOriente(mat);
  cout<<endl;

  afficherDegresOriente(mat);
return 1; 
  
}