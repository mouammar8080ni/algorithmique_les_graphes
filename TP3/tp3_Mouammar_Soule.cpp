#include <iostream>
#include <fstream>
using namespace std;

#include "types.hpp"


/* pré-définition des fonctions utilisées dans ce module
 * Le code des fonctions est donnée après la fonction main
 */
void creerMatrice(MatriceAdjacence &m, int taille);
void effacerMatrice(MatriceAdjacence &mat);
void afficher(MatriceAdjacence mat);
bool charger(char *nom, MatriceAdjacence &mat);
void afficherEnLargeur(MatriceAdjacence mat, Couleur *coul, int *dist, int *parent);
void parcoursEnLargeur(MatriceAdjacence mat, int sommetDepart, Couleur *coul,int *dist, int *parent);

void afficherCheminVers(int sf, int *parent) ;


// ===== PROGRAMME PRINCIPAL ===

int main(int argc, char *argv[]){
  //declaration des varaibles etc
  
  MatriceAdjacence mat = {0,NULL};
  
  int *dis = new int[mat.ordre];
  int *parent = new int[mat.ordre];
  Couleur *coul = new Couleur[mat.ordre];
  
  if(argc != 3){
    cout << "Erreur indice sommet de depart \n";
    return -1;
  }
 for(int i=0; i < argc; i++){
   cout<<argv[i];
   cout<<endl;
 }
  
  if(!charger(argv[1], mat)) return -1;
   int sommetDepart = stoi(argv[2]);
   if(sommetDepart < 0 || sommetDepart >= mat.ordre){
     return -1;
  }

  afficher(mat);
  parcoursEnLargeur( mat, sommetDepart,coul,dis,parent);
  afficherEnLargeur(mat,coul,dis,parent);

  for (int x = 0; x < mat.ordre; x++) {
      if (x!= sommetDepart && coul[x]==NOIR) {
          cout << "chemin vers " <<x <<" = ";
          afficherCheminVers(x, parent);
      cout << endl;
    }
     else {
       cout<< " pas de chemin de "<<sommetDepart<<" vers " <<x<<endl;
     }
}

 // effacerMatrice(mat);
  
  return 1;
}



void creerMatrice(MatriceAdjacence &m, int taille){
  // raz éventuelle de la matrice
  if(m.lignes!=nullptr) delete m.lignes;
  // initialisation du nombre de lignes/colonnes de la matrice
  m.ordre = taille;
  // allocation mémoire du tableau de lignes
  m.lignes = new Maillon*[taille];
  // initialisation de chaque ligne à "vide"
  for(int i=0; i<taille; i++) m.lignes[i]=nullptr;
}


void effacerMatrice(MatriceAdjacence &mat){
  for(int l=0; l<mat.ordre; l++){// effacer chaque ligne
    while(mat.lignes[l]!=nullptr){// tq la ligne n'est pas vide
      // effacer le premier élément qui s'y trouve
      Maillon *cour = mat.lignes[l];// 1er élément de la liste
      mat.lignes[l] = cour->suiv;// élément suivant éventuel
      delete cour; // effacer le 1er élement courant
    }
  }
  // effacer le tableau de lignes
  delete mat.lignes;
  mat.lignes = nullptr;
  // raz de la taille
  mat.ordre = 0;
      
}


//Chargement du fichier
bool charger(char *nom, MatriceAdjacence &mat){
  ifstream in;
  
  in.open(nom, std::ifstream::in);
  if(!in.is_open()){
    printf("Erreur d'ouverture de %s\n", nom);
    return false;
  }

  int taille;
  in >> taille;

  // créer la matrice
  creerMatrice(mat, taille);
  
  int v; // coefficient lu

  for(int l=0; l<mat.ordre; l++){ // lire et créer une ligne complète
    Maillon *fin=nullptr;// pointeur vers la fin d'une liste chaînée
    for(int c=0; c<mat.ordre; c++){ // lire et créer chaque colonne de la ligne courante
      in >> v;// lecture du coefficient (0 ou 1)
      if(v!=0){// créer un maillon et l'insérer en fin de liste
	// créer un nouveau maillon
	Maillon *nouveau = new Maillon;
	nouveau->col = c;
	nouveau->coef = v;
	nouveau->suiv = nullptr;
	// insérer le maillon en fin de liste
	if(fin!=nullptr){// il y a déjà des éléments dans la liste
	  fin->suiv = nouveau;// insertion en fin
	  fin = nouveau;// maj du pointeur vers le dernier élément de la liste
	}else{// c'est le premier coefficient de la liste
	  mat.lignes[l] = nouveau;// ajout au début de la liste
	  fin = nouveau;// maj du pointeur vers le dernier élément de la liste
	}
      }// if - rien à faire si v vaut 0
    }// for c
  }// for l

  in.close();
  return true;
}


//afficher une matrice
void afficher(MatriceAdjacence mat){
  // affichage de chacune des lignes
  for(int l=0; l<mat.ordre; l++){// affichage de la ligne l
    int c=0;
    Maillon *mcur=mat.lignes[l];
    while(c<mat.ordre){
      if(mcur==nullptr){// le coefficients de la ligne >=c sont nuls
	cout << "0 ";
	c++;
      }else if(mcur->col != c){
	// on est sur un coefficient nul, qui se trouve avant c
	cout << "0 ";
	c++;
      }else{// afficher le coefficient
	cout << mcur->coef << " ";
	mcur = mcur->suiv;
	c++;
      }   
    }// while
    cout << endl;// fin de la ligne l
  }// for
}

// Ajout des autres fonctions 
/* fonction qui initialise un fil FIFO à nul
  et prend en parametre un fil*/
void initiliserFIFO(Fifo &file){

    file.in = NULL;
    file.out = NULL;
}

//Fonction qui verifie si la file est vide ou pas
bool estVide(Fifo file){

    if(file.in == NULL && file.out == NULL) return true;
    else return false;
}

//Fonction d'insertion en tete de liste d'un FIFO
void ajouter(Fifo &file,int v){

    //creation de l'élément à ajouter
    MaillonEntier *eltInserer = new MaillonEntier;
    eltInserer->valeur = v;
    eltInserer->prec = NULL;
    eltInserer->suiv = NULL;

  //Verifions si la fil est vide pour commencer à            // initilaiser 
  if(estVide(file)){
     file.in = eltInserer;
     file.out = eltInserer;
  }
    // si le fil n'est pas vide, on commence les   
    //operations 
  else {
      file.in->suiv = eltInserer;
      eltInserer->prec = file.out;
      file.out =eltInserer;
  }
    
}

//Fonction pour retirer u element sur un fil
int retirer(Fifo &file){
     if(estVide(file)) return -1;

  // Valeur a retirer
  int valRetirer = file.in->valeur;
  //L'element a retirer sur le maillon 
  MaillonEntier *eltRetirer = file.out;

  //verifions si l'entré est egale a la sortie pour         
 //eliminer en premier 
  if(file.in == file.out){
    initiliserFIFO(file);
  }

  else {
      file.out = file.out->prec;
      file.out->suiv = nullptr;
      delete eltRetirer;
  }

  return valRetirer;
}



// code pour le parcours en largeur 
void parcoursEnLargeur(MatriceAdjacence mat, int sommetDepart, Couleur *coul,int *dist, int *parent){
    // declarations de la file
   Fifo file;
   initiliserFIFO(file);
    //initiliastion de tous les sommets 
   int cmptr = 0;//compteur de deplacment
   while(cmptr <= mat.ordre){

      coul[cmptr] = BLANC;
      dist[cmptr] = INFINI;
      parent[cmptr] = INDEFINI;
      cmptr++;
   }

  //initialisations du sommet de depart
  coul[sommetDepart] = GRIS;
  dist[sommetDepart] = 0;
  parent[sommetDepart] = INDEFINI;
  ajouter(file, sommetDepart);

  while (!estVide(file)){
    int valRetirer = retirer(file);
    
    Maillon *neighbor = mat.lignes[valRetirer];
    while ( neighbor != NULL){

        if(coul[neighbor->col] == BLANC){
           coul[neighbor->col] = GRIS;
           dist[neighbor->col] = dist[valRetirer] + 1;
           parent[neighbor->col] = valRetirer;
           ajouter(file, neighbor->col);
        }
      neighbor = neighbor->suiv;
    }
    
   coul[valRetirer] = NOIR;
  }
}

// afficher de la matrice &
void afficherEnLargeur(MatriceAdjacence mat, Couleur *coul, int *dist, int *parent){

  cout<<" couleurs  : ";
   for (int i = 0; i < mat.ordre; i++){
      if(coul[i] == BLANC){
         cout<<" B ";
      }else cout<<" N ";
     
   }
   cout<<endl;
    cout<<" distances : ";
   for (int i = 0; i < mat.ordre; i++){
     if(dist[i] == INFINI)
      cout<<" X ";
      else cout<<" "<<dist[i]<<" ";
   }
    cout<<endl;
 cout<<" parents   : ";
   for(int i=0; i< mat.ordre; i++){
      if(parent[i] == INDEFINI){
         cout<<" X ";
      } else cout<<" "<<parent[i]<<" ";
   }
  cout<<endl;
}

// void afichage recursive des chemin d'un indice vers les autres
void afficherCheminVers(int sf, int *parent) {
  
    if (parent[sf] == INDEFINI) {
      cout<<sf<<" ";
      return;
    } else {
      afficherCheminVers(parent[sf], parent);
      cout<<sf<<" ";
    }
}