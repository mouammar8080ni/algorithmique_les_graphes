#include <fstream>
#include <iostream>
using namespace std;

#include "types.hpp"

struct MaillonPile {
  int valeur;
  MaillonPile *dessus; // element inferieur de la pile
};

struct Pile {
  MaillonPile *sp; // sommet de la pile
};

/* declaration des fonctions utilisées dans ce module
 * Le code des fonctions est donnée après la fonction main
 */
void creerMatrice(MatriceAdjacence &m, int taille);
void effacerMatrice(MatriceAdjacence &mat);
void afficher(MatriceAdjacence mat);
bool charger(char *nom, MatriceAdjacence &mat);
void ParcoursEnProfondeurRecursive(MatriceAdjacence mat, int sommet,Couleur *coul, int *parent) ;
void afficherEnProfondeur(MatriceAdjacence mat, Couleur *coul, int *parent);
void parcoursEnprofondeur(MatriceAdjacence mat, int sommet, Couleur *coul, int *parent);
void afficherCheminVers(int sf, int *parent);
void initialiser(Pile &pile);
bool estVide(Pile p);
void empiler(Pile &p);
int depiler(Pile &p);




// ===== PROGRAMME PRINCIPAL ===

int main(int argc, char *argv[]) {
  // declaration des varaibles etc
  int sommetDepart = stoi(argv[2]);
  MatriceAdjacence mat = {0, NULL};

  int *parent = new int[mat.ordre];
  Couleur *coul = new Couleur[mat.ordre];

  if (argc != 3) {
    cout << "Erreur indice sommet de depart \n";
    return -1;
  }
  
  if (!charger(argv[1], mat))
    return -1;
  if(sommetDepart < 0 || sommetDepart >= mat.ordre) return -1;
 else {
  cout<<"La Matrice : "<<endl;
  afficher(mat);
  cout<<endl;
  
  cout<<" partie recursive : "<<endl;
  ParcoursEnProfondeurRecursive(mat, sommetDepart, coul,parent);
  afficherEnProfondeur(mat, coul, parent);
  cout<<endl;
  
  cout<<" Sans recursive : "<<endl;
  parcoursEnprofondeur(mat, sommetDepart, coul,parent);
  afficherEnProfondeur(mat, coul, parent);
   cout<<endl;
  for (int x = 0; x < mat.ordre; x++) {
    if (x != sommetDepart && coul[x] == NOIR) {
      cout << "chemin vers " << x << " = ";
      afficherCheminVers(x, parent);
      cout << endl;
    } else {
      cout << "pas de chemin de " << sommetDepart << " vers " << x << endl;
    }
  }
}
  // effacerMatrice(mat);

  return 1;
}

void creerMatrice(MatriceAdjacence &m, int taille) {
  // raz éventuelle de la matrice
  if (m.lignes != nullptr)
    delete m.lignes;
  // initialisation du nombre de lignes/colonnes de la matrice
  m.ordre = taille;
  // allocation mémoire du tableau de lignes
  m.lignes = new Maillon *[taille];
  // initialisation de chaque ligne à "vide"
  for (int i = 0; i < taille; i++)
    m.lignes[i] = nullptr;
}

void effacerMatrice(MatriceAdjacence &mat) {
  for (int l = 0; l < mat.ordre; l++) { // effacer chaque ligne
    while (mat.lignes[l] != nullptr) {  // tq la ligne n'est pas vide
      // effacer le premier élément qui s'y trouve
      Maillon *cour = mat.lignes[l]; // 1er élément de la liste
      mat.lignes[l] = cour->suiv;    // élément suivant éventuel
      delete cour;                   // effacer le 1er élement courant
    }
  }
  // effacer le tableau de lignes
  delete mat.lignes;
  mat.lignes = nullptr;
  // raz de la taille
  mat.ordre = 0;
}

// Chargement du fichier
bool charger(char *nom, MatriceAdjacence &mat) {
  ifstream in;

  in.open(nom, std::ifstream::in);
  if (!in.is_open()) {
    printf("Erreur d'ouverture de %s\n", nom);
    return false;
  }

  int taille;
  in >> taille;

  // créer la matrice
  creerMatrice(mat, taille);

  int v; // coefficient lu

  for (int l = 0; l < mat.ordre; l++) { // lire et créer une ligne complète
    Maillon *fin = nullptr; // pointeur vers la fin d'une liste chaînée
    for (int c = 0; c < mat.ordre;
         c++) {     // lire et créer chaque colonne de la ligne courante
      in >> v;      // lecture du coefficient (0 ou 1)
      if (v != 0) { // créer un maillon et l'insérer en fin de liste
        // créer un nouveau maillon
        Maillon *nouveau = new Maillon;
        nouveau->col = c;
        nouveau->coef = v;
        nouveau->suiv = nullptr;
        // insérer le maillon en fin de liste
        if (fin != nullptr) {  // il y a déjà des éléments dans la liste
          fin->suiv = nouveau; // insertion en fin
          fin = nouveau; // maj du pointeur vers le dernier élément de la liste
        } else {         // c'est le premier coefficient de la liste
          mat.lignes[l] = nouveau; // ajout au début de la liste
          fin = nouveau; // maj du pointeur vers le dernier élément de la liste
        }
      } // if - rien à faire si v vaut 0
    }   // for c
  }     // for l

  in.close();
  return true;
}

// afficher une matrice
void afficher(MatriceAdjacence mat) {
  // affichage de chacune des lignes
  for (int l = 0; l < mat.ordre; l++) { // affichage de la ligne l
    int c = 0;
    Maillon *mcur = mat.lignes[l];
    while (c < mat.ordre) {
      if (mcur == nullptr) { // le coefficients de la ligne >=c sont nuls
        cout << "0 ";
        c++;
      } else if (mcur->col != c) {
        // on est sur un coefficient nul, qui se trouve avant c
        cout << "0 ";
        c++;
      } else { // afficher le coefficient
        cout << mcur->coef << " ";
        mcur = mcur->suiv;
        c++;
      }
    }             // while
    cout << endl; // fin de la ligne l
  }               // for
}

// Ajout des autres fonctions
// parcours en recursivité 
void explorerDepuis(MatriceAdjacence mat, int sommet, Couleur *coul, int *parent){

   coul[sommet] = GRIS;
    Maillon *maillon = mat.lignes[sommet];
   while(maillon !=NULL){
     if(coul[maillon->col] == BLANC){
        parent[maillon->col] = sommet;
        explorerDepuis(mat,maillon->col,coul,parent);
     }
     maillon = maillon->suiv;
   }

  coul[sommet] =NOIR;
   
}

// la fonction recursive du parcours en profondeur 
void ParcoursEnProfondeurRecursive(MatriceAdjacence mat, int sommet,Couleur *coul, int *parent){
  
    for(int x=0; x < mat.ordre; x++){
       coul[x] = BLANC;
       parent[x] = INDEFINI;
    }
  
   for(int j=0; j < mat.ordre; j++){
      if(coul[j] == BLANC){
        explorerDepuis(mat,j,coul,parent);
      }  
    }    
}


// afficher du parcours en profondeur
void afficherEnProfondeur(MatriceAdjacence mat, Couleur *coul, int *parent) {

  cout << " couleurs  : ";
  for (int i = 0; i < mat.ordre; i++) {
    if (coul[i] == BLANC) {
      cout << " B ";
    } else
      cout << " N ";
  }
  cout << endl;
  cout << " parents   : ";
  for (int i = 0; i < mat.ordre; i++) {
    if (parent[i] == INDEFINI) {
      cout << " X ";
    } else
      cout << " " << parent[i] << " ";
  }
  cout << endl;
}

/* fonction qui initialise un pile à nul
  et prend en parametre un pile*/
void initiliserPile(Pile &pile) { pile.sp = nullptr; }

// verifons si la pile est vide
bool estVide(Pile p) { return p.sp == nullptr; }

// Empiler une pile an ajoutant une valeur au sommet
void empiler(Pile &p, int val) {

  // creation de la valeur a ajouter
  MaillonPile *valAjouter = new MaillonPile;
  valAjouter->valeur = val;
  valAjouter->dessus = nullptr;
  if (estVide(p)) {
    p.sp = valAjouter;
  }

  else {
    p.sp->dessus = valAjouter;
  }
}

// Fonction pour deplier
int depiler(Pile &p) {

  if (estVide(p))
    return -1;
  // Valeur a retirer
  int valRetirer = p.sp->valeur;
  // L'element a retirer sur le maillon
  MaillonPile *eltRetirer = p.sp;
  p.sp = eltRetirer->dessus;

  delete eltRetirer;
  return valRetirer;
}

//Fonction parcours en profondeur sans recursvité 
void parcoursEnprofondeur(MatriceAdjacence mat, int sommet, Couleur *coul, int *parent){

   // creation d'une pile et l'initialiser
    Pile pile;
   initiliserPile(pile);
  //empiler le premier sommetDepart de la pile
   empiler(pile, sommet);

  // tant que notre pile n'est pas vide, depile, puis on     
  //regarde ces voisins adjacent et puis on empile 
   while(!estVide(pile)){
      int valActuel = depiler(pile);
   
      Maillon *maillon = mat.lignes[valActuel];
     
      while(maillon != nullptr){ 
        
        if(coul[maillon->col] == BLANC){
          coul[maillon->col] = GRIS;
          parent[maillon->col] = valActuel;
          empiler(pile, maillon->col);
          
        } 
         maillon = maillon->suiv;
      }
      coul[valActuel] = NOIR;
   } 
}


// void afichage recursive des chemin d'un indice vers les autres
void afficherCheminVers(int sf, int *parent) {
  
    if (parent[sf] == INDEFINI) {
      cout<<sf<<" ";
      return;
    } else {
      afficherCheminVers(parent[sf], parent);
      cout<<sf<<" ";
    }
}

