#include "types.hpp"
#include "inclusion.hpp"
#include "ecritureFichier.hpp"
// --------------------------------------------------------------------
// définition des fonctions liées à la gestion d'un labyrinthe
// --------------------------------------------------------------------

// à compléter ...

// --------------------------------------------------------------------
// définition des fonctions liées à l'écriture dans les fichiers SVG
// --------------------------------------------------------------------

bool ouvrirFichierSVG(const string &nomFichier, ofstream &out, int largeur,
                      int hauteur) {
  out.open(nomFichier);

  if (!out.is_open()) {

    cout << "erreur d'ouverture du fichier de dessin " << nomFichier << endl;
    return false;
  }

  // sortie de l'entête
  out << "<?xml version=\"1.0\" encoding=\"utf-8\"?>" << endl;
  out << "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" ";
  out << "width=\"" << largeur << "\" height=\"" << hauteur << "\">" << endl;
  // out <<"margin=\""<<MARGE <<endl;
  return true;
}

void fermerFichierSVG(ofstream &out) {
  // fermeture de la balise svg
  out << "</svg>" << endl;

  out.close();
}

void text(ofstream &out, int x, int y, int size, const string &txt,
          const string &color) {

  out << "<text x=\"" << x << "\" y=\"" << y << "\"";
  out << " font-family=\"Verdana\" font-size=\"" << size << "\"";
  out << " text-anchor=\"middle\" ";
  out << " dominant-baseline=\"middle\" ";
  out << "fill=\"" << color << "\" >" << endl;
  out << txt << endl;
  out << "</text>" << endl;
}

void ligne(ofstream &out, int x1, int y1, int x2, int y2, const string &color,
           int width) {
  out << "<line x1=\"" << x1 << "\" y1=\"" << y1 << "\"";
  out << " x2=\"" << x2 << "\" y2=\"" << y2;
  out << "\" stroke=\"" << color << "\"";
  out << " stroke-width=\"" << width << "\""
      << " />" << endl;
}

void rect(ofstream &out, int x, int y, int width, int height,
          const string &color) {
  out << "<rect width=\"" << width << "\" height=\"" << height << "\"";
  out << " x=\"" << x << "\" y=\"" << y << "\" fill=\"" << color << "\"";
  out << " />" << endl;
}

// == 2eme partie du projet ==
// --------------------------------------------------------------------
// Fonction de conversion d'une valeur entière en chaîne de caractères
// --------------------------------------------------------------------


string intToString(int v) {
  stringstream s;
  s << v;
  return s.str();
}



// initialisation des labyrinthe
void initialiserLabyrinthe(labyrinthe &laby, int largeur, int hauteur) {
  ofstream out;
  // dessinerGrille(out,largeur,hauteur);
  laby.largeur = largeur;
  laby.hauteur = hauteur;
  laby.cell = new int[largeur * hauteur];
  laby.mursV = new bool[(largeur + 1) * hauteur];
  laby.mursH = new bool[largeur * (hauteur + 1)];

  for (int i = 0; i < hauteur * largeur; i++) {
    laby.cell[i] = i;
  }

  for (int j = 0; j < (largeur + 1) * hauteur; j++) {
    laby.mursV[j] = true;
  }
  for (int x = 0; x < largeur * (hauteur + 1); x++) {
    laby.mursH[x] = true;
  }
}


// Fonction pour effacer les zonnes memoires utiliser
//  pour effacer les zones memoires utiliser pour le labyrinthe
void effacerLabyrinthe(labyrinthe &laby) {
  delete laby.cell;
  laby.cell = NULL;
  delete laby.mursH;
  laby.mursH = NULL;
  delete laby.mursV;
  laby.mursV = NULL;
}



// Dessinons la grille pour le labyrinthe
void dessinerLabyrinthe(labyrinthe &laby, const string &nomFichier) {
  ofstream out;
  int height = (laby.hauteur * CELLSIZE) + 2 * MARGE;
  int width = (laby.largeur * CELLSIZE) + 2 * MARGE;

  if (ouvrirFichierSVG(nomFichier, out, width, height)) {
    rect(out, 0, 0, width, height, "white");

    // murs horizontale
    for (int i = 0; i < laby.hauteur + 1; i++) {
      for (int j = 0; j < laby.largeur; j++) {
        int x1 = j * CELLSIZE + MARGE;
        int y1 = i * CELLSIZE + MARGE;
        int x2 = (j + 1) * CELLSIZE + MARGE;
        int y2 = i * CELLSIZE + MARGE;
        if (laby.mursH[((i * laby.largeur) + j)] == true) {
          // cout << laby.mursH[(i * laby.largeur) + j] << " ";
          ligne(out, x1, y1, x2, y2, "black", 2);
        }
      }
      cout << endl;
    }

    cout << endl;
    // murs verticale
    for (int i = 0; i < laby.hauteur; i++) {
      for (int j = 0; j < laby.largeur + 1; j++) {
        int x1 = j * CELLSIZE + MARGE;
        int y1 = i * CELLSIZE + MARGE;
        int x2 = j * CELLSIZE + MARGE;
        int y2 = (i + 1) * CELLSIZE + MARGE;
        //  cout << laby.mursV[(i * (laby.largeur + 1)) + j] << " ";
        if (j == 0 || j == laby.largeur ||
            laby.mursV[(i * (laby.largeur + 1)) + j] == true) {
          ligne(out, x1, y1, x2, y2, "black", 2);
        }
      }
      cout << endl;
    }

  }
  fermerFichierSVG(out);
}


// fonction pour fusionner deux chemin en un seul
// CAD les identifiants du 2eme chemin a celui du premier
void fussionnerIdentifiant(labyrinthe &laby, int firstWay, int secondWay) {
  for (int i = 0; i < laby.largeur * laby.hauteur; i++) {
    if (laby.cell[i] == secondWay) {
      laby.cell[i] = firstWay;
    }
  }
}



// Fonction qui permet de retirer un mur si c'est possible
void removeWall(labyrinthe &laby, int ligne, int colone, int mur) {
  // mur de gauche vertical
  if (mur == 0) {
    laby.mursV[ligne * (laby.largeur + 1) + colone] = false;

    // laby.mursV[ligne - 1] = false;
  }

  // mur de droite vertical , accÃ©es a l'indice de la cellue + 1
  else if (mur == 1) {
    laby.mursV[ligne * (laby.largeur + 1) + colone + 1] = false;

  }
  // mur de haut, horizontal
  else if (mur == 2) {
    // laby.mursH[ligne - laby.largeur] = false;
    laby.mursH[ligne * laby.largeur + colone] = false;
  }

  // mur de bas  horizontal
  else if (mur == 3) {
    // cout << "ligne -> " << cellule << endl;
    // cout << laby.cell[cellule] << " " << endl;
    laby.mursH[(ligne + 1) * laby.largeur + colone] = false;
  }
}



// Fonction qui genere le labyrinthe
void genererLabyrinthe(labyrinthe &laby) {
  srand(time(NULL));
  int murRemovetotal =
      (laby.largeur * laby.hauteur) - 1; // nombre de murs à supprimer

  while (murRemovetotal > 0) {

    int colone = rand() % laby.largeur;
    int ligne = rand() % laby.hauteur;
    int cellueAleatoire = ligne * laby.largeur + colone;
    int murAleatoire = rand() % 4;

    // == mur gauche ==
    if (murAleatoire == 0 && colone > 0) {
      // int leftCellule = indiceCellule(laby, colone - 1, ligne);
      int leftCellule = ligne * laby.largeur + colone - 1;

      if (laby.cell[cellueAleatoire] != laby.cell[leftCellule]) {
        fussionnerIdentifiant(laby, laby.cell[cellueAleatoire],
                              laby.cell[leftCellule]);
        removeWall(laby, ligne, colone, murAleatoire);
        murRemovetotal--;
      }

      // === mur de droite ===
    } else if (murAleatoire == 1 && colone < laby.largeur - 1) {
      int rightCellule = ligne * laby.largeur + colone + 1;

      if (laby.cell[cellueAleatoire] != laby.cell[rightCellule]) {
        fussionnerIdentifiant(laby, laby.cell[cellueAleatoire],
                              laby.cell[rightCellule]);
        removeWall(laby, ligne, colone, murAleatoire);
        murRemovetotal--;
      }

      // === mur de haut ===
    } else if (murAleatoire == 2 && ligne > 0) {
      int topCellule = (ligne - 1) * laby.largeur + colone;

      if (laby.cell[cellueAleatoire] != laby.cell[topCellule]) {
        fussionnerIdentifiant(laby, laby.cell[cellueAleatoire],
                              laby.cell[topCellule]);
        removeWall(laby, ligne, colone, murAleatoire);
        murRemovetotal--;
      }

      // === mur de bas ===
    } else if (murAleatoire == 3 && ligne < laby.hauteur - 1) {
      int bottomCellule = (ligne + 1) * laby.largeur + colone;

      if (laby.cell[cellueAleatoire] != laby.cell[bottomCellule]) {
        fussionnerIdentifiant(laby, laby.cell[cellueAleatoire],
                              laby.cell[bottomCellule]);
        removeWall(laby, ligne, colone, murAleatoire);
        murRemovetotal--;
      }
    }
  }
}
