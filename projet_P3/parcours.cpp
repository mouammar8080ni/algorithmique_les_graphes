#include "types.hpp"
#include "inclusion.hpp"
#include "ecritureFichier.hpp"

// === 3eme partie du projet ===
void creerMatrice(MatriceAdjacence &mat, int taille) {

  mat.ordre = taille;
  mat.lignes = new Maillon *[mat.ordre];
  for (int i = 0; i < mat.ordre; i++) {
    mat.lignes[i] = nullptr;
  }
}

void effacerMatrice(MatriceAdjacence &mat) {
  Maillon *maillon;
  for (int i = 0; i < mat.ordre; i++) {
    while (mat.lignes[i] != nullptr) {
      maillon = mat.lignes[i];
      mat.lignes[i] = mat.lignes[i]->suiv; // avancer le pointeur de ligne
      delete maillon;
    }
  }
  delete[] mat.lignes;
  mat.ordre = 0;
}

void afficherMatrice(MatriceAdjacence &mat) {

  for (int i = 0; i < mat.ordre; i++) {

    Maillon *maillon = mat.lignes[i];
    cout << " noeud " << i;
    while (maillon != nullptr) {
      cout << " (" << maillon->l << "," << maillon->c << ") : ";

      maillon = maillon->suiv;
    }
    cout << endl;
  }
}

void remplirMatrice(MatriceAdjacence &mat, labyrinthe &lab) {

  // parcours de la grille
  for (int i = 0; i < lab.largeur * lab.hauteur; i++) {

    int ligne = i / lab.largeur;  // trouver la ligne en question
    int colone = i % lab.largeur; // trouver la colone en question
    // on verifie sur les cellules accessible i
    Maillon *ptr = new Maillon;
    ptr->l = ligne;
    ptr->c = colone;
    ptr->suiv = nullptr;

    if (mat.lignes[i] == nullptr) {
      mat.lignes[i] = ptr;
    }

    // cellule de gauche
    if (lab.mursV[(i / lab.largeur) + i] == false) {
      Maillon *maillon = new Maillon;
      maillon->l = ligne;
      maillon->c = colone - 1;
      maillon->suiv = mat.lignes[i]->suiv;
      mat.lignes[i]->suiv = maillon;
    }

    // cellule de droite
    if (lab.mursV[(i / lab.largeur) + i + 1] == false) {

      Maillon *maillon = new Maillon;
      maillon->l = ligne;
      maillon->c = colone + 1;
      maillon->suiv = mat.lignes[i]->suiv;
      mat.lignes[i]->suiv = maillon;
    }

    // cellule de haut
    if (lab.mursH[i] == false) {

      Maillon *maillon = new Maillon;
      maillon->l = ligne - 1;
      maillon->c = colone;
      maillon->suiv = mat.lignes[i]->suiv;
      mat.lignes[i]->suiv = maillon;
    }

    // cellue de bas
    if (lab.mursH[i + lab.largeur] == false) {

      Maillon *maillon = new Maillon;
      maillon->l = ligne + 1;
      maillon->c = colone;
      maillon->suiv = mat.lignes[i]->suiv;
      mat.lignes[i]->suiv = maillon;
      cout << maillon->c << " " << maillon->l << endl;
    }
  }
}

// fonction pour dessiner la matrice avec une ligne
void dessinerMatrice(MatriceAdjacence &mat, labyrinthe &laby,
                     const string &nomFichier) {
  ofstream out;
  coordonnee c;
  int height = (laby.hauteur * CELLSIZE) + 2 * MARGE;
  int width = (laby.largeur * CELLSIZE) + 2 * MARGE;

  if (ouvrirFichierSVG(nomFichier, out, width, height)) {
    rect(out, 0, 0, width, height, "white");

    // murs horizontale
    for (int i = 0; i < laby.hauteur + 1; i++) {
      for (int j = 0; j < laby.largeur; j++) {
        int x1 = j * CELLSIZE + MARGE;
        int y1 = i * CELLSIZE + MARGE;
        int x2 = (j + 1) * CELLSIZE + MARGE;
        int y2 = i * CELLSIZE + MARGE;
        if (laby.mursH[((i * laby.largeur) + j)] == true) {
          // cout << laby.mursH[(i * laby.largeur) + j] << " ";
          ligne(out, x1, y1, x2, y2, "black", 2);
        }
      }
      cout << endl;
    }

    cout << endl;
    // murs verticale
    for (int i = 0; i < laby.hauteur; i++) {
      for (int j = 0; j < laby.largeur + 1; j++) {
        int x1 = j * CELLSIZE + MARGE;
        int y1 = i * CELLSIZE + MARGE;
        int x2 = j * CELLSIZE + MARGE;
        int y2 = (i + 1) * CELLSIZE + MARGE;
        //  cout << laby.mursV[(i * (laby.largeur + 1)) + j] << " ";
        if (j == 0 || j == laby.largeur ||
            laby.mursV[(i * (laby.largeur + 1)) + j] == true) {
          ligne(out, x1, y1, x2, y2, "black", 2);
        }
      }
      cout << endl;
    }

    // Parcourir le graphe et dessiner les arêtes

    for (int i = 0; i < mat.ordre; i++) {
      Maillon *maillon = mat.lignes[i]; // recuperation du somets

      // calcul des coordonnées du sommet en question
      int x1 = maillon->c * CELLSIZE + CELLSIZE / 2 + MARGE;
      int y1 = maillon->l * CELLSIZE + CELLSIZE / 2 + MARGE;

      // on parcour pour chercher ses voisins et dessiners les arrets
      while (maillon != nullptr) {

        int x2 = maillon->c * CELLSIZE + CELLSIZE / 2 + MARGE;
        int y2 = maillon->l * CELLSIZE + CELLSIZE / 2 + MARGE;
        ligne(out, x1, y1, x2, y2, "red", 1);
        maillon = maillon->suiv;
      }
    }
  }
  fermerFichierSVG(out);
}



// saisi des coordonnées de l'utilsateur
void saisirCoordonnees(coordonnee &deb, coordonnee &fin, int largeur,
                       int hauteur) {

  for (;;) { // permet de ressayer tant que c'est faut, il parcours jusqu'a
             // l'infini

    cout << " == Cordonnée case de depart == " << endl;
    cout << " x : ";
    cin >> deb.x;
    cout << " y : ";
    cin >> deb.y;
    if (deb.x >= 0 && deb.y >= 0 && deb.x < largeur && deb.y < hauteur)
      break;
    else
      cout << " Cordonnée Invalide: RESSAYER !!" << endl;
  }

  // permet de ressayer tant que c'est faut, il parcours jusqu'a l'infini
  for (;;) {

    cout << " == Cordonnée case de d'arrivée == " << endl;
    cout << " x : ";
    cin >> fin.x;
    cout << " y : ";
    cin >> fin.y;
    if (fin.y >= 0 && fin.y >= 0 && fin.x < largeur && fin.y < hauteur)
      break;
    else
      cout << " Cordonnée Invalide: RESSAYER !!" << endl;
  }

  cout << deb.x << " " << deb.y << endl;
  cout << fin.x << " " << fin.y << endl;
}


// Ajout des autres fonctions
/* fonction qui initialise un fil FIFO à nul
  et prend en parametre un fil*/
void initiliserFIFO(Fifo &file) {

  file.in = NULL;
  file.out = NULL;
}


// Fonction qui verifie si la file est vide ou pas
bool estVide(Fifo file) {

  if (file.in == NULL && file.out == NULL)
    return true;
  else
    return false;
}


// Fonction d'insertion en tete de liste d'un FIFO
void ajouter(Fifo &file, int v) {

  // creation de l'élément à ajouter
  MaillonEntier *eltInserer = new MaillonEntier;
  eltInserer->valeur = v;
  eltInserer->prec = NULL;
  eltInserer->suiv = NULL;

  // Verifions si la fil est vide pour commencer à            // initilaiser
  if (estVide(file)) {
    file.in = eltInserer;
    file.out = eltInserer;
  }
  // si le fil n'est pas vide, on commence les
  // operations
  else {
    file.in->suiv = eltInserer;
    eltInserer->prec = file.out;
    file.out = eltInserer;
  }
}



// Fonction pour retirer un element sur un fil
int retirer(Fifo &file) {

  if (estVide(file))
    return -1;

  // Valeur à retirer
  int valRetirer = file.out->valeur;
  MaillonEntier *eltRetirer = file.out; // Élément à retirer sur le maillon

  // Mise à jour de l'élément "out" de la file
  file.out = file.out->prec;

  if (file.out != NULL) {
    file.out->suiv = NULL;
  }

  else {
    file.in = NULL;
  }

  delete eltRetirer;
  return valRetirer;
}


// code pour le parcours en largeur
void parcourEnLargeur(MatriceAdjacence mat, int largeur, int sommetDepart,
                      Couleur *coul, int *dist, int *parent) {
  // declarations de la file
  Fifo file;
  initiliserFIFO(file);
  // initiliastion de tous les sommets
  int cmptr = 0; // compteur de deplacment
  while (cmptr < mat.ordre) {

    coul[cmptr] = BLANC;
    dist[cmptr] = INFINI;
    parent[cmptr] = INDEFINI;
    cmptr++;
  }

  // initialisations du sommet de depart
  coul[sommetDepart] = GRIS;
  dist[sommetDepart] = 0;
  parent[sommetDepart] = INDEFINI;
  ajouter(file, sommetDepart);

  while (!estVide(file)) {
    int valRetirer = retirer(file);
    Maillon *neighbor = mat.lignes[valRetirer];
    while (neighbor != NULL) {
      int coord = (neighbor->l * largeur) + neighbor->c;
      if (coul[coord] == BLANC) {
        coul[coord] = GRIS;
        dist[coord] = dist[valRetirer] + 1;
        parent[coord] = valRetirer;
        ajouter(file, coord);
      }
      neighbor = neighbor->suiv;
    }

    coul[valRetirer] = NOIR;
  }
}


// afficher de la matrice &
void afficherEnLargeur(MatriceAdjacence mat, Couleur *coul, int *dist,
                       int *parent) {

  cout << " couleurs  : ";
  for (int i = 0; i < mat.ordre; i++) {
    if (coul[i] == BLANC) {
      cout << " B ";
    } else
      cout << " N ";
  }
  cout << endl;
  cout << " parents   : ";
  for (int i = 0; i < mat.ordre; i++) {
    if (parent[i] == INDEFINI) {
      cout << " X ";
    } else
      cout << " " << parent[i] << " ";
  }
  cout << endl;

  cout << " dist      : ";
  for (int i = 0; i < mat.ordre; i++) {
    if (dist[i] == INFINI) {
      cout << " X ";
    } else
      cout << " " << dist[i] << " ";
  }
  cout << endl;
}


// fonction pour le calcul du chemin
chemin calculerChemin(MatriceAdjacence &mat, coordonnee deb, coordonnee fin,
                      int largeur) {

  cout << endl;
  // calcul de l'indice de ma case de depart et de ma case d'arrivé
  int indiceDepart = (deb.x * largeur) + deb.y;
  int indiceArrivee = (fin.x * largeur) + fin.y;

  // creations des tablaux necessaire pour le parcours en largeurs
  int *dist = new int[mat.ordre];
  int *parent = new int[mat.ordre];
  Couleur *coul = new Couleur[mat.ordre];
  parcourEnLargeur(mat, largeur, indiceDepart, coul, dist, parent);
  afficherEnLargeur(mat, coul, dist, parent);

  // creation du chemin entre le point de depart le point d'arrivée
  chemin way;
  way.lg = dist[indiceArrivee] + 1;
  // allouons la memoire dynamiquement pour le chemin
  way.etape = new coordonnee[way.lg];
  // compteur pour avancer sur la ligne
  int cmptr = way.lg - 1;
  while (indiceArrivee != INFINI) {
    int cordX = indiceArrivee / largeur;
    int cordY = indiceArrivee % largeur;
    way.etape[cmptr] = {cordX, cordY};
    indiceArrivee = parent[indiceArrivee];
    cmptr--;
  }

  return way;
}

// dessiner solution
void desinerSolution(const labyrinthe &laby, const chemin &way,
                     const string &nomFichier) {

  ofstream out;
  coordonnee c;
  int height = (laby.hauteur * CELLSIZE) + 2 * MARGE;
  int width = (laby.largeur * CELLSIZE) + 2 * MARGE;

  if (ouvrirFichierSVG(nomFichier, out, width, height)) {
    rect(out, 0, 0, width, height, "white");

    // murs horizontale
    for (int i = 0; i < laby.hauteur + 1; i++) {
      for (int j = 0; j < laby.largeur; j++) {
        int x1 = j * CELLSIZE + MARGE;
        int y1 = i * CELLSIZE + MARGE;
        int x2 = (j + 1) * CELLSIZE + MARGE;
        int y2 = i * CELLSIZE + MARGE;
        if (laby.mursH[((i * laby.largeur) + j)] == true) {
          // cout << laby.mursH[(i * laby.largeur) + j] << " ";
          ligne(out, x1, y1, x2, y2, "black", 2);
        }
      }
      cout << endl;
    }

    cout << endl;
    // murs verticale
    for (int i = 0; i < laby.hauteur; i++) {
      for (int j = 0; j < laby.largeur + 1; j++) {
        int x1 = j * CELLSIZE + MARGE;
        int y1 = i * CELLSIZE + MARGE;
        int x2 = j * CELLSIZE + MARGE;
        int y2 = (i + 1) * CELLSIZE + MARGE;
        //  cout << laby.mursV[(i * (laby.largeur + 1)) + j] << " ";
        if (j == 0 || j == laby.largeur ||
            laby.mursV[(i * (laby.largeur + 1)) + j] == true) {
          ligne(out, x1, y1, x2, y2, "black", 2);
        }
      }
      cout << endl;
    }

    // tracer la solution

    for (int i = 0; i < way.lg - 1; i++) {
      coordonnee c1 = way.etape[i];
      coordonnee c2 = way.etape[i + 1];
      int x1 = c1.y * CELLSIZE + MARGE + CELLSIZE / 2;
      int y1 = c1.x * CELLSIZE + MARGE + CELLSIZE / 2;
      int x2 = c2.y * CELLSIZE + MARGE + CELLSIZE / 2;
      int y2 = c2.x * CELLSIZE + MARGE + CELLSIZE / 2;
      ligne(out, x1, y1, x2, y2, "blue", 1);
    }
  }
  fermerFichierSVG(out);
}
