#include <cstdlib>
#include <ctime>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
using namespace std;

#define CELLSIZE 50 // taille d'un côté d'une case du labyrinthe
#define MARGE 10    // marge du dessin
#define TEXTSIZE 10 // taille d'affichage du texte
#define HAUTEUR 10  // valeur par defaut de la hauteur
#define LARGEUR 10  // valeur par defaut de la largeur
