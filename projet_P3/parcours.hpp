#include <iostream>
#include <fstream>
using namespace std;
// prédefintion des fonctions liées au matrices adjacent
void remplirMatrice(MatriceAdjacence &mat, labyrinthe &lab);
void creerMatrice(MatriceAdjacence &mat, int taille);
void effacerMatrice(MatriceAdjacence &mat);
void afficherMatrice(MatriceAdjacence &mat);
void ajouterMaillon(MatriceAdjacence &mat, Maillon *maillon, int index);
void dessinerMatrice(MatriceAdjacence &mat, labyrinthe &laby,const string &nomFichier);
void saisirCoordonnees(coordonnee &deb, coordonnee &fin, int largeur,int hauteur);
chemin calculerChemin(MatriceAdjacence &mat, coordonnee deb, coordonnee fin, int largeur);
void desinerSolution(const labyrinthe &laby, const chemin &way, const string &nomFichier) ;
