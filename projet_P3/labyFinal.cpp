#include "types.hpp"
#include "labyrinthe.hpp"
#include "parcours.hpp"
#include "inclusion.hpp"
#include <iostream>



// --------------------------------------------------------------------
// programme principal
// --------------------------------------------------------------------

int main(int argc, char *argv[]) {

  // declaration des varaibles
  ofstream out;
  labyrinthe laby;
  int hauteur, largeur;
  coordonnee deb, fin;
  MatriceAdjacence mat;
  chemin way;
  string fichier = "laby.svg";

  // si l'utilisateur entre un seul argument
  if (argc == 1) {
    hauteur = HAUTEUR;
    largeur = LARGEUR;
  }

  // argument donnéer
  if (argc == 3) {
    hauteur = atoi(argv[1]);
    largeur = atoi(argv[2]);
  } else {
    cout << "Argument Manquant ou plus erreur syntaxe !!!!" << endl;
  }

  // generation du labyrinthe vide
  initialiserLabyrinthe(laby, hauteur, largeur);
  genererLabyrinthe(laby);
  dessinerLabyrinthe(laby, fichier);
  // affichage de la grille
  for (int i = 0; i < laby.hauteur; i++) {
    for (int j = 0; j < laby.largeur; j++) {

      cout << laby.cell[i * laby.largeur + j] << " ";
    }
    cout << endl;
  }

  // creation de la matrice et parcours du graphe
  int taille = hauteur * largeur;
  creerMatrice(mat, taille);
  remplirMatrice(mat, laby);
  dessinerMatrice(mat, laby, "graphe.svg");
  afficherMatrice(mat);

  // affichage en trouvant le chemin le plus petit
  saisirCoordonnees(deb, fin, largeur, hauteur);
  way = calculerChemin(mat, deb, fin, hauteur);
  desinerSolution(laby, way, "solution.svg");

  return 0;
}

