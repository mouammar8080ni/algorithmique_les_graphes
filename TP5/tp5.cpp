#include <iostream>
#include <fstream>
using namespace std;

#include "types.hpp"



/* pré-définition des fonctions utilisées dans ce module
 * Le code des fonctions est donnée après la fonction main
 */
void creerMatrice(MatriceAdjacence &m, int taille);
void effacerMatrice(MatriceAdjacence &mat);
void afficher(MatriceAdjacence mat);
bool charger(char *nom, MatriceAdjacence &mat);
void rechercheCheminMinimum(MatriceAdjacence mat,int sommetDepart, double *dist, int *parent, Couleur *coul);
int sommetBlancMinimum(MatriceAdjacence mat,int sommetDepart, double *dist, int *parent, Couleur *coul);
void afficherCheminVers(int sf, int *parent);




  
int main(int argc, char *argv[]){
  
  if(argc!=3){
    cout << "Syntaxe : " << argv[0] << " <file>  <indice_sommet_depart>" << endl;
    return -1;
  }

  MatriceAdjacence mat;
   

  if(!charger(argv[1], mat)) return -1;

  int sommetDepart = atoi(argv[2]);
  if(sommetDepart <0 || sommetDepart >= mat.ordre){
    cout << "indice de sommet " << sommetDepart << " incorrect ! ";
    cout << "valeurs autorisées dans [0," << mat.ordre-1 << "]" << endl;
    effacerMatrice(mat);
    return -1;
  }

   double *dist = new double[mat.ordre];
   int *parent = new int[mat.ordre];
   Couleur *coul = new Couleur[mat.ordre];
   rechercheCheminMinimum(mat,sommetDepart,dist, parent, coul);
   //cout<<sommetBlancMinimum(mat,sommetDepart,dist, parent, coul)<<endl;

  for (int x = 0; x < mat.ordre; x++) {
      if (x!= sommetDepart && coul[x]==NOIR) {
          cout << "chemin vers " <<x <<"( lg = "<<dist[x]<<" ) = ";
          afficherCheminVers(x, parent);
      cout << endl;
   }
     
}


  //afficher(mat);


  
  return 1;
}


void creerMatrice(MatriceAdjacence &m, int taille){
  // initialisation du nombre de lignes/colonnes de la matrice
  m.ordre = taille;
  // allocation mémoire du tableau de lignes
  m.lignes = new Maillon*[taille];
  // initialisation de chaque ligne à "vide"
  for(int i=0; i<taille; i++) m.lignes[i]=nullptr;
}

void effacerMatrice(MatriceAdjacence &mat){
  for(int l=0; l<mat.ordre; l++){// effacer chaque ligne
    while(mat.lignes[l]!=nullptr){// tq la ligne n'est pas vide
      // effacer le premier élément qui s'y trouve
      Maillon *cour = mat.lignes[l];// 1er élément de la liste
      mat.lignes[l] = cour->suiv;// élément suivant éventuel
      delete cour; // effacer le 1er élement courant
    }
  }
  // effacer le tableau de lignes
  delete mat.lignes;
  // raz de la taille
  mat.ordre = 0;
      
}

bool charger(char *nom, MatriceAdjacence &mat){
  ifstream in;
  
  in.open(nom, std::ifstream::in);
  if(!in.is_open()){
    printf("Erreur d'ouverture de %s\n", nom);
    return false;
  }

  int taille;
  in >> taille;

  // créer la matrice
  creerMatrice(mat, taille);
  

  for(int l=0; l<mat.ordre; l++){ // lire et créer une ligne complète
    Maillon *fin=nullptr;
    int col;// numéro de colonne lu
    int val; // valeur du coefficient lu
    in >> col;
    while(col!=-1){// lire les données de la colonne
      in >> val;
      // créer un maillon et l'insérer en fin de liste
      Maillon *nouveau = new Maillon;
      nouveau->col = col;
      nouveau->coef = val;
      nouveau->suiv = nullptr;
      if(fin!=nullptr){// il y a déjà des éléments dans la liste
          fin->suiv = nouveau;// insertion en fin
          fin = nouveau;// maj du pointeur vers le dernier élément de la liste
          }else{// c'est le premier coefficient de la liste
          mat.lignes[l] = nouveau;// ajout au début de la liste
          fin = nouveau;// maj du pointeur vers le dernier élément de la liste
      }
      // lire la colonne suivante
      in >> col;
    }
  }// for l

  in.close();
  return true;
}





void afficher(MatriceAdjacence mat){
  // affichage de chacune des lignes
  for(int l=0; l<mat.ordre; l++){// affichage de la ligne l
    int c=0;
    Maillon *mcur=mat.lignes[l];
    while(c<mat.ordre){
      if(mcur==nullptr){// le coefficients de la ligne >=c sont nuls
        cout << "0 ";
        c++;
      }else if(mcur->col != c){
	// on est sur un coefficient nul, qui se trouve avant c
      cout << "0 ";
      c++;
      }else{// afficher le coefficient
        cout << mcur->coef << " ";
        mcur = mcur->suiv;
        c++;
      }   
    }// while
    cout << endl;// fin de la ligne l
  }// for
}


// rechercher du sommet blanc minimum 
int sommetBlancMinimum(MatriceAdjacence mat,int sommetDepart, double *dist, int *parent, Couleur *coul){

    double valMin = INFINI;
    int cmptr = 0;
    int sommetMin = -1;
    while (cmptr < mat.ordre){
        if(coul[cmptr] == BLANC){
            if(dist[cmptr] <= valMin){
                valMin = dist[cmptr];
                sommetMin = cmptr;
            }
        }
        cmptr++;
    }
    return sommetMin;
}


// recherche du chemin minimum 
void rechercheCheminMinimum(MatriceAdjacence mat,int sommetDepart, double *dist, int *parent, Couleur *coul){

    //initialisation des sommets 
    int cmptr = 0;
    while( cmptr < mat.ordre){
      coul[cmptr] = BLANC;
      dist[cmptr] = INFINI;
      parent[cmptr] = INDEFINI;
      cmptr++;

    }


    // initialisation du sommet de depart 
    dist[sommetDepart] = 0;
    parent[sommetDepart] = INDEFINI;
    int n= 0;
    while( n < mat.ordre){

        int i = sommetBlancMinimum(mat, sommetDepart, dist, parent, coul);
       
            for(Maillon *maillon = mat.lignes[i]; maillon != nullptr; maillon = maillon->suiv){
             // cout<<maillon->col <<" "<<maillon->coef<<endl;
                if(coul[maillon->col] == BLANC){
                 // cout<<maillon->col <<" "<<maillon->coef<<endl;
                    if(dist[maillon->col] > dist[i] + maillon->coef){
                        dist[maillon->col] = dist[i] + maillon->coef;
                        parent[maillon->col] = i;
                    }
                }
            }
        coul[i] = NOIR;
        n++;
    }
}

//Affichage info 

void afficherCheminVers(int sf, int *parent){

    if(parent[sf] == INDEFINI){
       cout<<sf<<" ";
       return ;
    }
    else{
      afficherCheminVers(parent[sf], parent);
      cout<<sf<<" ";
    }
}


