#include <iostream>
#include <fstream>
using namespace std;

#include "types.hpp"
 ifstream fichierIn;
 ofstream fichierOut;



/* pré-définition des fonctions utilisées dans ce module
 * Le code des fonctions est donnée après la fonction main
 */
void creerMatrice(MatriceAdjacence &m, int taille);
void effacerMatrice(MatriceAdjacence &mat);
void afficher(MatriceAdjacence mat);
bool charger(char *fichierin, MatriceAdjacence &mat);
void convertirFormat(MatriceAdjacence &mat, char *fichierOut);
void afficherMatConverti(MatriceAdjacence mat);


 /* ==== PROGRAMME PRINCIPAL === */ 
int main(int argc, char *argv[]){

  // verification des arguments en paremetre
  if(argc!=3){
    cout << "arguments manquant" << endl;
    return -1;
  }

  // declaration des variables 
  MatriceAdjacence mat;
  char *fichierOut = argv[2];

 if(!charger(argv[1], mat)) return -1;

    cout<<"======= Avant Converstion: mat intiale ======= "<<endl;
    afficher(mat);

    cout<<"========  Conversion en format new  ========"<<endl;
    convertirFormat(mat,fichierOut);
    afficherMatConverti(mat);

  return 1;
}



// fonction d'utilisation 
void creerMatrice(MatriceAdjacence &m, int taille){
  // initialisation du nombre de lignes/colonnes de la matrice
  m.ordre = taille;
  // allocation mémoire du tableau de lignes
  m.lignes = new Maillon*[taille];
  // initialisation de chaque ligne à "vide"
  for(int i=0; i<taille; i++) m.lignes[i]=nullptr;
}

void effacerMatrice(MatriceAdjacence &mat){
  for(int l=0; l<mat.ordre; l++){// effacer chaque ligne
    while(mat.lignes[l]!=nullptr){// tq la ligne n'est pas vide
      // effacer le premier élément qui s'y trouve
      Maillon *cour = mat.lignes[l];// 1er élément de la liste
      mat.lignes[l] = cour->suiv;// élément suivant éventuel
      delete cour; // effacer le 1er élement courant
    }
  }
  // effacer le tableau de lignes
  delete mat.lignes;
  // raz de la taille
  mat.ordre = 0;
      
}


/* une fonction qui charge la matrice
  a partir d'un lecture d'un fichier
  et renvoie false s'il le trouve pas
*/

bool charger(char *fichierin, MatriceAdjacence &mat) {
  
  fichierIn.open(fichierin, ios::in);
  if (!fichierIn.is_open()) {
    cout << "Erreur d'ouvertur de fichier !!!";
    return false;
  }

  else {
    fichierIn >> mat.ordre;
    creerMatrice(mat, mat.ordre);

    for (int i = 0; i < mat.ordre; i++) {
      for (int j = 0; j < mat.ordre; j++) {
        int coeficient;
        fichierIn >> coeficient;

        if (coeficient != 0) {
          Maillon *maillon = new Maillon;
          maillon->col = j;
          maillon->coef = coeficient;

          if (mat.lignes[i] == nullptr) {
            mat.lignes[i] = maillon;
          } else {
            Maillon *ptr = mat.lignes[i];
            Maillon *prec = nullptr;

            while (ptr != nullptr) {
              prec = ptr;
              ptr = ptr->suiv;
            }
            prec->suiv = maillon;
        }
      }
    }
  }
    }
  fichierIn.close();
  return true;
}




void afficher(MatriceAdjacence mat){
  // affichage de chacune des lignes
  for(int l=0; l<mat.ordre; l++){// affichage de la ligne l
    int c=0;
    Maillon *mcur=mat.lignes[l];
    while(c<mat.ordre){
        if(mcur==nullptr){// le coefficients de la ligne >=c sont nuls
    
          	cout << "0 ";
          	c++;
        }else if(mcur->col != c){
  	       // on est sur un coefficient nul, qui se trouve avant c
            cout << "0 ";
            c++;
        }else{// afficher le coefficient
          
          	cout <<mcur->coef << " ";
          	mcur = mcur->suiv;
          	c++;
        }   
    }// while
    cout << endl;// fin de la ligne l 
  }// for
}


/* Fonction pour la converstion d'une matrice sur un fichier vers un nouveau fichier en sauvagardant juste les valeurs !=null et la colone*/
void convertirFormat(MatriceAdjacence &mat, char *nom){

    fichierOut.open(nom, fstream::out);
    if(!fichierOut.is_open()){
        cout<<" Erreur de conversion !!!"<<endl;
        return ;
    }

    else {
        fichierOut<<mat.ordre<<endl;
        int i = 0;

        while(i < mat.ordre){
          
                Maillon *maillon = mat.lignes[i];
                while(maillon != nullptr){
                   fichierOut<< maillon->col<<" "<< maillon->coef<<" ";
                    maillon = maillon->suiv;
                }
                fichierOut<< " -1 "<< endl;
          i++;
        }
    }

    fichierOut.close();
 
}

// affichage des matrice converti 
void afficherMatConverti(MatriceAdjacence mat){

  for(int i =0; i<mat.ordre; i++){
      Maillon *maillon = mat.lignes[i];
       while( maillon != nullptr){
           cout<< maillon->col<<" "<<maillon->coef<<" ";
          maillon = maillon->suiv;
       }
      cout<<"-1"<<endl;
    }
}


